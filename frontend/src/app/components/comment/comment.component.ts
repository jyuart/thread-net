import { Component, Input } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { Observable, empty, Subject } from 'rxjs';
import { AuthenticationService } from 'src/app/services/auth.service';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { LikeService } from 'src/app/services/like.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { DialogType } from 'src/app/models/common/auth-dialog-type';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private snackBarService: SnackBarService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public likeComment(isLike: boolean) {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp, isLike)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser, isLike)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public showAllLikers() {
        var likers = [];
        for (let react of this.comment.reactions) {
            if (react.isLike === true)
                likers.push(react.user.userName);
        }
        return likers.join("\r\n");
    }

    public showAllDislikers() {
        var likers = [];
        for (let react of this.comment.reactions) {
            if (react.isLike === false)
                likers.push(react.user.userName);
        }
        return likers.join("\r\n");
    }

    public allLikes() {
        let likes: number = this.comment.reactions.filter(function (obj) { return obj.isLike !== false }).length;
        return likes;
    }

    public allDislikes() {
        let likes: number = this.comment.reactions.filter(function (obj) { return obj.isLike === false }).length;
        return likes;
    }
}
